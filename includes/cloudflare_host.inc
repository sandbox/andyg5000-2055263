<?php
class CloudflareHost {
  protected $cloudflareUserEmail;
  protected $cloudflareUserPassword;
  protected $cloudflareUsername;
  protected $cloudflareUserKey;
  protected $unique_id;
  protected $cloudflareServer;
  protected $cloudflareHostKey;
  protected $postData;
  protected $cloudflareDomain;
  public $cloudflareResponse;
  public $error;


  public function __construct($server, $key) {
    $this->cloudflareServer = $server;
    $this->cloudflareHostKey = $key;
  }

  public function setUserEmail($email) {
    $this->cloudflareUserEmail = $email;
  }

  public function setUserPassword($password) {
    $this->cloudflareUserPassword = $password;
  }

  public function setDomain($domain) {
    $this->cloudflareDomain = $domain;
  }

  public function userCreate() {
    // Validate the request.
    $required = array('cloudflareUserEmail', 'cloudflareUserPassword');
    if (!$this->validateRequest('userCreate', $required)) {
      return;
    }

    $this->postData = array(
      'host_key' => $this->cloudflareHostKey,
      'act' => 'user_create',
      'cloudflare_email' => $this->cloudflareUserEmail,
      'cloudflare_pass' => $this->cloudflareUserPassword,
    );

    return $this->send();
  }

  public function zoneSet($resolve_to, $subdomains) {
    // Validate the request.
    $required = array('cloudflareUserKey', 'cloudflareDomain');
    if (!$this->validateRequest('zoneSet', $required)) {
      return;
    }

    $this->postData = array(
      'act' => 'zone_set',
      'user_key' => $this->cloudflareUserKey,
      'zone_name' => $this->cloudflareDomain,
      'resolve_to' => $resolve_to,
      'subdomains' => $subdomains,
    );

    return $this->send();
  }

  public function setUserKey($key) {
    $this->cloudflareUserKey = $key;
  }

  public function getUserKey() {
    $required = array('cloudflareUserEmail');
    if (!$this->validateRequest('getUserKey', $required)) {
      return;
    }

    $this->userLookup();
    $this->send();
    if (!empty($this->cloudflareResponse->data->user_key)) {
      $this->cloudflareUserKey = $this->cloudflareResponse->data->user_key;
    }

    return $this->send();
  }

  public function userLookup() {
    // Validate the request.
    $required = array('cloudflareUserEmail');
    if (!$this->validateRequest('userLookup', $required)) {
      return;
    }

    // Set up the request.
    $this->postData = array(
      'act' => 'user_lookup',
      'cloudflare_email' => $this->cloudflareUserEmail,
    );

    return $this->send();
  }

  public function zoneLookup() {
    // Validate the request.
    $required = array('cloudflareUserKey', 'cloudflareDomain');
    if (!$this->validateRequest('zoneLookup', $required)) {
      return;
    }

    $this->postData = array(
      'act' => 'zone_lookup',
      'zone_name' => $this->cloudflareDomain,
      'user_key' => $this->cloudflareUserKey,
    );

    return $this->send();
  }

  public function zoneList($offset = 0, $limit = 10) {
    $this->postData = array(
      'act' => 'zone_list',
      'limit' => $limit,
      'offest' => $offset,
    );

    if (!empty($this->cloudflareDomain)) {
      $this->postData['zone_name'] = $this->cloudflareDomain;
    }

    return $this->send();
  }

  public function zoneDelete() {
    $required = array('cloudflareUserKey', 'cloudflareDomain');

    if (!$this->validateRequest('zoneDelete', $required)) {
      return;
    }

    $this->postData = array(
      'act' => 'zone_delete',
      'zone_name' => $this->cloudflareDomain,
      'user_key' => $this->cloudflareUserKey,
    );

    return $this->send();
  }

  public function send() {
    $this->postData['host_key'] = $this->cloudflareHostKey;
    $options = array(
      'method' => 'POST',
      'data' => drupal_http_build_query($this->postData),
      'timeout' => 15,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );

    $response = drupal_http_request($this->cloudflareServer, $options);

    $this->cloudflareResponse = new stdClass();

    if (!empty($response->data)) {
      $data = json_decode($response->data);
      if (empty($data->err_code)) {
        $this->cloudflareResponse->status = 'success';
        $this->cloudflareResponse->data = $data->response;
      }
      else{
        $this->cloudflareResponse->status = 'error';
        $this->cloudflareResponse->data = $data->msg;
      }
    }
    else{
      $this->cloudflareResponse->status = 'error';
      $this->cloudflareResponse->data = NULL;
    }

    return $this->cloudflareResponse;
  }

  private function validateRequest($method, $required) {
    foreach ($required as $field) {
      if (empty($this->$field)) {
        $this->error = t('Missing field @field when calling @method', array('@field' => $field, '@method' => $method));
        drupal_set_message($this->error, 'error');
        return FALSE;
      }
      return TRUE;
    }
  }

}
